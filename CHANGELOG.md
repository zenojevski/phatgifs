CHANGELOG
=========

### 0.1
- Initial version.

### 0.2
- GIFs are now transcoded using Gfycat.
- Added direct support for Gfycat URLs.

### 0.3
- Rewritten the Player API.
- New player selector system.
- Sync and cueing are more precise now.
- Gfycat transcoding falls back to GIF now.
- Rewritten the viewer overlay to be more informative.

##### 0.3.1
- Cleaned the stylesheets, removed a lot of cruft.
- Refined the general look & feel a bit.
- The viewer now takes care of fitting the media to the viewport.
- New logo effect with correct amount of zazz.

##### 0.3.2
- Fixed an issue during recueing with Popcorn.

##### 0.3.3
- Better error reporting and tracking for all players.
- Overall style refinements for viewer overlay and editor fields.

##### 0.3.4
- Temporarily switched to playing `webm` instead of `h264` for gfycats to improve the experience on Firefox.

##### 0.3.5
- Using the correct Gfycat transcode API endpoint

### 0.4
- Added support for [imgur's `gifv` format](http://imgur.com/blog/2014/10/09/introducing-gifv)

##### 0.4.1
- New popcorn.js version.
