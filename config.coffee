exports.config =

  files:

    javascripts:
      joinTo:
        'scripts/app.js': /^app/
        'scripts/vendor.js': /^(vendor|bower_components)\//
      after: [
        'popcorn.complete.js'
      ]

    stylesheets:
      joinTo:
        'styles/app.css': /^app\/styles/
        'styles/vendor.css': /^(vendor|bower_components)\//

    templates:
      joinTo: 'scripts/app.js'
