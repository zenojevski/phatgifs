NOT_INSTALLED = grep -q "State: not installed"

usage:
	echo deploy; \
	echo build; \
	echo serve

.reqs_nodejs:
	@if aptitude show software-properties-common | $(NOT_INSTALLED); then \
		sudo apt-get install -y software-properties-common; \
	fi
	@if aptitude show build-essential | $(NOT_INSTALLED); then \
		sudo apt-get install -y build-essential; \
	fi
	@if aptitude show nodejs | $(NOT_INSTALLED); then \
		sudo add-apt-repository -y ppa:chris-lea/node.js; \
		sudo apt-get update; \
		sudo apt-get install -y nodejs; \
	fi
	@if aptitude show optipng | $(NOT_INSTALLED); then \
		sudo apt-get install -y optipng; \
	fi
	@if aptitude show libjpeg-turbo-progs | $(NOT_INSTALLED); then \
		sudo apt-get install -y libjpeg-turbo-progs; \
	fi

.reqs_npm:
	@npm install

deploy: .reqs_nodejs .reqs_npm

build: requirements
	@npm run-script build
