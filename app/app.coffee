Base = require 'base'
Settings = require 'settings'
{Combo} = require 'models/combo'
Viewer = require 'views/viewer'
Editor = require 'views/editor'


class Application extends Base.Application

	id: 'phatgifs'
	className: 'phatgifs-app'
	template: require 'templates/application'

	routes:
		'' :'show'

	events:
		'keydown': 'onActivity'
		'mousemove': 'onActivity'
		'click a[href]': 'onVisitLink'

	appEvents:
		'show': 'show'

	constructor: ->
		@onActivity = _.throttle @onActivity, Settings.interface.activityThrottle
		super

	initialize: (options) ->
		($ window).on 'resize', => @trigger 'app:resize'
		@addInitializer @initModels
		@addInitializer @initViews
	initModels: (options, done) ->
		@model = new Combo
		@listenTo @model,
			'change': @onModelChange
		done()
	initViews: (options, done) ->
		@viewer = new Viewer
			model: @model
		@editor = new Editor
			model: @model
		@dim yes
		done()
	afterRender: ->
		@attach @viewer
		@attach @editor

	isPristine: ->
		not ((@model.get 'video') and (@model.get 'sound'))

	onModelChange: ->
		params =
			v:    @model.get 'video'
			s:    @model.get 'sound'
			cue:  @model.get 'cue'
			vcue: @model.get 'vcue'
		app.router.setRoute 'show', params
		@editor.edit() if @isPristine()
		@onActivity()

	show: (params) ->
		params = {} unless params
		@editor.edit no
		# Maintain compatibility with old query parameters
		# while also introducing some shortcuts
		@model.set
			video: params.v  or params.video or params.picture
			sound: params.s  or params.sound
			cue:   params.c  or params.cue
			vcue:  params.vc or params.vcue

	onVisitLink: (event) ->
		a = event.currentTarget
		if a.origin is location.origin
			app.router.navigate a.pathname, trigger:yes
			false

	onActivity: ->
		clearTimeout @_dimmer if @_dimmer
		@_dimmer = setTimeout (=>@dim yes), Settings.interface.dimmingTimeout
		@dim no
	dim: (dimmed=yes) ->
		if @isPristine()
			dimmed = no
		unless dimmed?
			dimmed = @$el.hasClass 'is-dimmed'
		@$el.toggleClass 'is-dimmed', dimmed


module.exports = window.app = new Application

# We need the router to know he'll have pushstate
# because we're using reverse during rendering but
# before actually starting the history
Base.history._hasPushState = yes
Base.history._hasHashChange = no
Base.history.root = '/'

app.start().attachTo('body')

Base.history.start
	pushState:  Base.history._hasPushState
	hashChange: Base.history._hasHashChange
	root:       Base.history.root
