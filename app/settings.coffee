settings = {}

settings.static_url = '/static'


settings.interface =
	# Specifies the time of no interactivity required before the
	# interface begins dimming.
	dimmingTimeout: 2500
	# Throttling for interface activity events.
	activityThrottle: 500


settings.viewer =
	# Throttling for window resize events.
	resizeThrottle: 14


settings.editor =
	# The amount of time that must be waited before changes
	# to the contents will be applied.
	debounce: 1000


settings.popcorn =
	# The amount of time used to distantiate the end cue from the end
	# of the content, to work around the imprecisions of the player.
	eventTimeCorrection: 0.25
	# If the player view is not connected to the DOM, wait this
	# amount of time before trying again.
	DOMWaitingTime: 1000

# Local settings
try
	_.extend settings, require './local_settings'
catch e
	no

module.exports = settings