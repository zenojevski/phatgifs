Base = require 'base'


class Combo extends Base.Model
	defaults:
		video: null
		sound: null
		cue: null
		vcue: null

class Combos extends Base.Collection
	model: Combo


module.exports =
	Combo: Combo
	Combos: Combos