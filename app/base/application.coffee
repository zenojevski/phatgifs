View = require './view'


class Application extends Giraffe.App
	constructor: (options={}) ->
		# TODO: Move this into defaultOptions
		options.templateStrategy = 'jst'
		super options
	serialize: ->
		_.extend (View::serialize.call this),
			_app: this


module.exports = Application