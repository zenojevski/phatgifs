base =

	Class:          require './class'

	Model:          require './model'
	Collection:     require './collection'

	View:           require './view'
	Tabs:           require './tabs'
	Panes:          require './panes'
	CollectionView: require './collection-view'

	Router:         require './router'
	Application:    require './application'
	history:        Backbone.history


module.exports = base