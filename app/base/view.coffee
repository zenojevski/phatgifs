Utils = require 'utils'
BaseView = Giraffe.View


domEvents = [
	'click'
	'dblclick'
]

domNamespace = 'view'


class View extends BaseView
	domEvents: domEvents

	constructor: (options={}) ->
		options.templateStrategy = 'jst'
		Utils.objects.pickOptions this, options, [
			'domEvents', ]
		for method in ['show', 'hide', 'open', 'close']
			Utils.events.wrap this, method
		super options

	delegateEvents: ->
		super
		events = _.map @domEvents, (event) -> "#{event}.#{domNamespace}"
		@$el.on (events.join ' '), @onDomEvent
		this
	undelegateEvents: ->
		super
		@$el.off ".#{domNamespace}", @onDomEvent
		this
	onDomEvent: (event) =>
		@trigger event.type, @model

	setModel: (model) ->
		@model = model
		@hasRendered = no
	setCollection: (collection) ->
		@collection = collection
		@hasRendered = no

	isInDOM: ->
		@$el.closest('body').length > 0
	isVisible: ->
		@$el.is(':visible')
	open: ->
		@$el.addClass 'open'
	close: ->
		@$el.removeClass 'open'
	show: (done) ->
		@$el.show()
		done?()
		this
	hide: (done) ->
		@$el.hide()
		done?()
		this
	toggle: (value, done) ->
		show = if value? then value else not @isVisible()
		if show then @show done else @hide done
		this

	serialize: ->
		_.extend (@model?.toJSON() || {}),
			_model: @model
			_view: this,
			_app: @app,
			$: Utils

module.exports = Giraffe.View = View