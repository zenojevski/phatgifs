View = require './view'


class Tabs extends View
	"""
	Implements selecting and deselecting tabs.
	"""
	className: 'tabs'
	itemSelector: '.item'
	defaultItem: no

	getItems: ->
		items = {}
		for item in @$ @itemSelector
			items[item.href[1..]] = ($ item)

	deselect: ->
		@currentItem.removeClass 'selected'
		@currentItem = no

	select: (id) ->
		@deselect() if @currentItem
		@currentItem = (@getItems().find("[href='##{id}']") or @getItems().first())
			.addClass 'selected'

	afterRender: ->
		@select()


module.exports = Tabs