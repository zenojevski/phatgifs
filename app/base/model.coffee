class Model extends Backbone.RelationalModel

	constructor: (attributes, options) ->
		Giraffe.configure this, options
		@app or= options?.app or Giraffe.app
		Giraffe.bindEventMap this, @app, @appEvents
		super

	fetch: (options={}) ->
		options.data = _.extend {}, options.data, @data? options
		super options


module.exports = Model