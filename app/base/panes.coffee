Tabs = require './tabs'


class Panes extends Tabs
	"""
	A navigation system which shows and hides panes.
	"""
	className: 'panes'
	tabSelector: '.pane'
	constructor: (@items, options) ->
		super options
		for name, pane of @items
			@attach pane
	getItems: ->
		@items
	select: (id) ->
		@currentItem
		@currentPane = id


module.exports = Panes