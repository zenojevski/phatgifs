class Collection extends Giraffe.Collection
	parse: (attrs) ->
		@meta = attrs.meta
		return attrs.objects ? attrs
	fetch: (options={}) ->
		options.data = _.extend {}, options.data, @data? options
		super options


module.exports = Collection