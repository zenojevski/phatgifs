View = require './view'


class CollectionView extends View
	itemClass: View
	itemClassName: no
	itemTemplate: no
	itemTagName: 'li'

	itemAutoRender: yes
	itemAutoAppend: yes

	itemContainer: -> @$el

	constructor: (options={}) ->
		@views = {}
		_.extend this, _.pick options, [
			'itemClass', 'itemClassName', 'itemTemplate', 'itemTagName',
			'itemAutoRender', 'itemAutoAppend' ]
		super
		@listenTo @collection,
			add: @onAdd
			remove: @onRemove
			reset: @onReset
	onAdd: (model) =>
		view = new @itemClass
			model: model
			className: @itemClassName
			tagName: @itemTagName
			template: @itemTemplate
		@listenTo view, 'all', (event, args...) =>
			@trigger "item:#{event}", view, args...
		@views[model.cid] = view
		view.$el.data
			view:view,
			model:model
		if @itemAutoRender
			view.render()
		if @itemAutoAppend
			view.$el.appendTo @itemContainer()
			view.trigger 'append'
		@trigger 'items:add', view
		return this
	onRemove: (model) =>
		delete (view = @views[model.cid])
		view.destroy()
		@stopListening view
		@trigger 'items:remove', view
		return this
	onReset: (models) =>
		@clear models
		@collection.each @onAdd
		return this
	clear: (newModels) =>
		view.destroy() for cid, view of @views
		if newModels?.length
			@itemContainer().empty()
		@views = {}
		return this
	render: ->
		@onReset()
		return this
	destroy: ->
		@clear()
		super


module.exports = CollectionView