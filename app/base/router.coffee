BaseRouter = Giraffe.Router
Utils = require 'utils'

class Router extends BaseRouter

	_prepareGroups: (str, optional=no) ->
		content = ''
		children = []
		for c, i in str
			if c is '('
				child = @_prepareGroups str[i+1..], yes
				_i = _i + child._i + 1
				children.push child
			else if c isnt ')'
				content += c
			else
				break
		{ content, children, optional, _i }

	_prepareTokens: (fragment, args) ->
		tokens = /([:*])(\w+)/g
		count = -1
		fill = (fragment) ->
			pruned = no
			content = fragment.content.replace tokens, (match, type, token) ->
				count += 1
				if pruned
					return ''
				# param
				if type is ':'
					r = args[0]?[token] ? args[count]
				# splat
				else if type is '*'
					splitArgs = args[count..]
					r = args[0]?[token] ? (splitArgs.join? '/')
					count += args.length
				# prune the entire parameter if it's empty and optional
				if not r and fragment.optional
					pruned = yes
				return r
			# Insert children
			if pruned
				return ''
			for child, i in fragment.children
				content += (fill child) or ''
			return content
		fill fragment, args

	reverse: (name, args...) ->
		for route, event of @triggers
			if event is name
				# FIXME: Try to call Base.history instead
				return @_prepareTokens (@_prepareGroups route), args

	getRoute: (appEvent, any...) ->
		if (route = super)?
			if (_.isObject params = any.pop())
				params = Utils.objects.clean params
				if params and (_.keys params).length
					route += "?#{$.param params}"
		return route

	setRoute: (appEvent, any...) ->
		if (route = @getRoute appEvent, any...)?
			Backbone.history.navigate route,
				trigger: no

	cause: (appEvent, any...) ->
		if (route = @getRoute appEvent, any...)?
			Backbone.history.navigate route,
				trigger: yes
		else
			@app.trigger appEvent, any...


module.exports = Giraffe.Router = Router