Base = require 'base'
Utils = require 'utils'
Settings = require 'settings'
Player = require './player'
{clamp} = Utils.math.clamp


# A phatgif combo viewer which creates and manages the video and the
# audio players, with looping.
module.exports =
class Viewer extends Base.View

	className: 'combo-viewer'
	template: require 'templates/viewer'

	ui:
		$content: '.combo-content'
		$video: '.combo-video-player'
		$sound: '.combo-sound-player'
		$statusMessage: '.status-message'

	appEvents:
		'app:resize': '_resize'
		'viewer:mute': 'mute'

	dataEvents:
		'change model': 'update'

	constructor: ->
		@_resize = _.throttle @_resize, Settings.viewer.resizeThrottle
		super

	initialize: ->
		@_video = @_createPlayer muted:yes
		@_sound = @_createPlayer muted:@muted
		@listenTo @_video, 'error', (r) => @_state 'error', r, 'video'
		@listenTo @_sound, 'error', (r) => @_state 'error', r, 'sound'

	mute: (muted=null) ->
		@muted = muted ? not @muted
		@_sound.mute muted

	# Parses a cue string into two separate cues
	parseCues: (cues) ->
		return unless cues
		[start, end] = cues.split '-'
		start: (Utils.time.parseCue start)
		end:   (Utils.time.parseCue end)

	_errorMessage: (component, reason) ->
		"""
		<div><strong>Could not load the #{component}.</strong></div>
		<div>#{reason}</div>
		"""

	_state: (state='', reason='', component) ->
		# Don't override the current state
		return if @$el.hasClass "combo-is-#{state}"
		@$el.removeClass 'combo-is-loading combo-is-error'
		@$el.addClass "combo-is-#{state}" if state
		switch state
			when 'error'
				@$statusMessage.html @_errorMessage component, reason
			else
				@$statusMessage.text reason


	update: ->
		@_state 'loading'
		{video, sound, cue, vcue} = @model.attributes
		# Update both players.
		@_updatePlayer @_video, video, vcue
		@_updatePlayer @_sound, sound, cue
		# If we have no video or sound, clear any statuses
		# and do nothing.
		unless video or sound
			return @_state()
		# If any of the players are erroring right now, handle that
		if @_video.error then return @_state 'error', @_video.error, 'video'
		if @_sound.error then return @_state 'error', @_sound.error, 'sound'
		# If we have both video and sound, we'll want to wait for both
		# players to signal load, and start them in sync.
		syncQueue = _.compact [@_video if video, @_sound if sound]
		# Wait for the players to be ready before uncovering.
		# FIXME: we don't wait for `play` because Popcorn does not send
		#        it for YouTube, apparently, after we seek. Investigate
		#        more on this.
		play = _.after (syncQueue.length), =>
			p.play() for p in syncQueue
			@_resize()
			@_state()
		for p in syncQueue
			p.once 'canplay', play
			p.pause()
			p.rewindToCue()

	_createPlayer: (options) ->
		new Player options

	_updatePlayer: (player, content=null, cues=null) ->
		unless content
			return player.unload()
		player.load content
		player.cue @parseCues cues

	afterRender: ->
		@_video.attachTo @$video
		@_sound.attachTo @$sound

	_resize: ->
		$content = @$video.find '.player-content *'
		outer = width:@$content.width(), height:@$content.height()
		unless inner = @_video.dimensions()
			# We don't have any reliable content dimensions, so
			# simply assign the full size of the viewport.
			$content.css outer
		else
			# Constrain the axis with the shortest ratio
			# and let the other free.
			ratio = inner.width/inner.height
			if (outer.width/inner.width) < (outer.height/inner.height)
				$content.css width:outer.width, height:outer.width/ratio
			else
				$content.css width:outer.height*ratio, height:outer.height
