Base = require 'base'
Utils = require 'utils'
EditorComponent = require './component'


module.exports =
class EditorPanel extends Base.View

	className: 'editor-panel'
	template: require 'templates/editor/panel'

	ui:
		$components: '.editor-components'
	events:
		'click button.start-editing': 'edit'
		'change [name="mute-sound"]': 'mute'
	appEvents:
		'viewer:mute': 'onMuteSound'

	afterRender: (options) ->
		for component in ['video', 'vcue', 'sound', 'cue']
			p = @[component] = new EditorComponent
				model: @model
				component: component
			p.attachTo @$components

	edit: (editing=yes) ->
		unless editing?
			editing = not @$el.hasClass 'is-editing'
		@$el.toggleClass 'is-editing', editing

	mute: (muted=yes) ->
		if muted.currentTarget
			muted = muted.currentTarget.checked
		app.trigger 'viewer:mute', muted

	onMuteSound: (muted=yes) ->
		(@$ '[name="mute-sound"]').get(0).checked = muted
