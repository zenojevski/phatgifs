Base = require 'base'
Utils = require 'utils'
settings = require 'settings'


module.exports =
class EditorComponent extends Base.View

	className: -> "editor-component component-#{@component}"
	template: require 'templates/editor/component'

	ui:
		$input: '[name="component"]'
	events:
		'paste  [name="component"]': 'onInputChange'
		'change [name="component"]': 'onInputChange'
		'keyup  [name="component"]': 'onInputChange'

	constructor: (options) ->
		@component = options.component
		@onInputChange = _.debounce @onInputChange, settings.editor.debounce
		super
	initialize: (options) ->
		@listenTo @model, "change:#{@component}", @update

	onInputChange: (event) =>
		@model.set @component, @$input.val()
	update: ->
		unless @$input.val() is (value = @model.get @component)
			@$input.val value
