Base = require 'base'
Utils = require 'utils'
PopcornPlayer = require './popcorn'


module.exports =
class VimeoPlayer extends PopcornPlayer

	className: 'player popcorn-player vimeo-player'

	@canPlay: (content) ->
		/\bvimeo\.com\b/.test content
