Base = require 'base'
Utils = require 'utils'
Settings = require 'settings'
Player = require './player'


# A player which uses the Popcorn.js library to play a variety of
# video and audio content, both files and video platforms.
module.exports =
class PopcornPlayer extends Player

	id: -> Utils.strings.uniqueToken()
	className: 'player popcorn-player'


# Content management
# ------------------

	# This class is not meant to play content directly.
	@canPlay: (content) ->
		no

	# Returns the currently loaded content.
	content: ->
		@_content

	_cleanURL: (content) ->
		# FIXME: We have to use the actual `mp4` extension for
		#        Popcorn to open a `.gifv` video.
		content = content.replace /\.gifv\b/, '.mp4'
		super content

	# Loads a content URL into the player. Will emit a `load` event
	# when the content is ready to be played, with the loaded content
	# as first argument.
	load: (content) ->
		# If we aren't attached to the DOM we can't yet deploy the
		# Popcorn player. So retry later, after render.
		unless @isInDOM()
			return _.delay (=> @load content), Settings.popcorn.DOMWaitingTime
		# Don't do anything if the url is invalid
		unless content = @_cleanURL content
			return
		# If the content is the same, don't do anything.
		if content is @content()
			return
		@ready = no
		unless @_player
			# We don't have a player yet, so create one.
			@_player = @_makePlayer content
			@_attachPlayer @_player
		else
			# Load the content into the existing player.
			@_player.media.src = content
		@_content = content

	# Unloads the current content from the player. Will emit an
	# `unload` event when the player is empty.
	unload: ->
		@ready = no
		@error = no
		@uncue()
		@_detachPlayer @_player if @_player
		delete @_player
		delete @_content


# Media control
# -------------

	# Returns the length of the currently loaded content.
	duration: ->
		return @_player?.duration() or 0

	# Returns the current playing time.
	currentTime: ->
		return @_player?.currentTime() or 0

	# Tells the player to start reproducing the content. Will trigger
	# a `play` event when the content has begun playing. Pass a `cue`
	# option to avoid playing from cue position.
	play: (options) ->
		@playing = yes
		@_player?.play()

	# Tells the player to pause the reproduction of the content. Will
	# trigger a `pause` event when the content has paused.
	pause: ->
		@playing = no
		@_player?.pause()

	# Seeks the player to a certain position in time. The position is
	# limited to the maximum duration of the content. Will emit a
	# `seek` event, with the current time as argument.
	seek: (time) ->
		@_player?.currentTime time

	# Mutes the player. Pass a boolean as argument to set the mute
	# switch to the given value, or null to flip the current setting.
	# Will trigger a `mute` or `unmute` event based on the value.
	mute: (muted=null) ->
		@muted = muted ? not @muted
		@_player?.muted @muted


# Cueing
# ------

	# Returns the time of the start cue.
	cueStartTime: ->
		@_cueStartTime ? 0

	# Returns the time of the end cue.
	cueEndTime: ->
		@_cueEndTime ? @duration()

	# Cues the player to make it loop through the given time range.
	cue: (cues={}) ->
		# If we can't access the media duration, we cannot cue now.
		unless @duration()
			@_delayedCues = cues
			return
		# Remove old cue events.
		@uncue()
		# If we have even one cue, we must stop relying on native
		# looping and begin manage player events.
		if cues?.start or cues?.end
			start = cues.start or 0
			end   = cues.end or @duration()
			# The safe end time is a timing hack to be able to detect
			# an ending cue, before the player autoloops.
			safeEndTime = @duration() - Settings.popcorn.eventTimeCorrection
			@_cueStartTime = Utils.math.clamp 0, start, @duration()
			@_cueEndTime   = Utils.math.clamp 0, end, safeEndTime
			# Cue the player on cue event, and store the cue ID.
			if @_player and @ready
				@_player.cue @_cueEndTime, => @rewindToCue()
				@_cueEvent = @_player.getLastTrackEventId()
			# Rewind the player if it was playing.
			@rewindToCue() if not @_player.paused()

	# Removes any set cues.
	uncue: ->
		if @_cueEvent and @_player
			@_player.removeTrackEvent @_cueEvent
		delete @_cueStartTime
		delete @_cueEndTime
		delete @_cueEvent


# Popcorn player
# --------------

	# A dictionary of events of interest of player instances.
	# Each one of them will be bound to the given method on self.
	@_playerEvents:
		'loadstart':   '_onPlayerLoadStart'
		'loadeddata':  '_onPlayerLoadedData'
		'canplay':     '_onPlayerCanPlay'
		'play':        '_onPlayerPlay'
		'playing':     '_onPlayerPlaying'
		'pause':       '_onPlayerPause'
		'seeked':      '_onPlayerSeeked'
		'ended':       '_onPlayerEnded'
		'emptied':     '_onPlayerEmptied'
		'error':       '_onPlayerError'
		'pluginerror': '_onPlayerError'

	# Creates a Popcorn Smart player which chooses which plugin to use
	# based on the content. Sets internal properties and binds to its
	# events. Returns the created player instance.
	_makePlayer: (content) ->
		# This creates a Popcorn player inside our $el.
		contentId = Utils.strings.uniqueToken()
		@$content.attr 'id', contentId
		player = Popcorn.smart contentId, content
		player.muted @muted
		player.loop yes # We try to loop natively when we can
		return player

	# Attaches the current player and binds to its events.
	_attachPlayer: (player) ->
		for event, method of @constructor._playerEvents
			player.on event, _.bind @[method], this
		player

	# Disposes of a player, unbinding from its events.
	_detachPlayer: (player) ->
		for event, method of @constructor._playerEvents
			player.off event
		player.destroy()
		@$el.empty()

	# Sent when loading of the media begins.
	_onPlayerLoadStart: ->
		@trigger 'loading'

	# The media’s metadata has finished loading; all attributes now
	# contain as much useful information as they’re going to.
	_onPlayerLoadedData: ->
		@ready = yes
		@error = no
		@trigger 'load'
		if cues = @_delayedCues
			# If we have a delayed cue in queue, load it now.
			delete @_delayedCues
			@cue cues
		else if @_cueStartTime or @_cueEndTime
			# If we have stading cue info, recue with that.
			@cue start:@_cueStartTime, end:@_cueEndTime

	# Sent when enough data is available that the media can be played,
	# at least for a couple of frames.
	_onPlayerCanPlay: ->
		# If we're not in cue, don't signal we're ready yet.
		# Seek and let the next `canplay` event bubble up.
		unless @isInCueRange()
			return @rewindToCue()
		# This would be the actual `canplay` signal.
		@trigger 'canplay'
		# Start the player if playing was requested but not going on.
		@_player.play() if @playing and @_player.paused()

	# Sent when the media begins to play (either for the first time,
	# after having been paused, or after ending and then restarting).
	_onPlayerPlaying: ->
		@trigger 'play'

	# Sent when the media begins to play (either for the first time,
	# after having been paused, or after ending and then restarting).
	_onPlayerPlay: ->

	# Sent when playback is paused.
	_onPlayerPause: ->
		@trigger 'pause'

	# Sent when a seek operation completes.
	_onPlayerSeeked: ->
		@trigger 'seek', @currentTime()

	# Sent when playback completes.
	_onPlayerEnded: ->
		# Either the video autoloops or a cue catches it before the
		# end, so we should never end up here!
		debugger if @cueEndTime()

	# Sent when playback completes.
	_onPlayerEmptied: ->
		@trigger 'unload'

	# Sent when the audio volume changes (both when the volume is set
	# and when the muted attribute is changed).
	_onPlayerVolumeChange: ->
		@muted = @_player.muted
		@trigger (if @muted then 'mute' else 'unmute')

	# Sent when an error occurs.
	_onPlayerError: ->
		switch (err = @_player.error).code
			when MediaError.MEDIA_ERR_ABORTED
				@_error 'Loading has been canceled.'
			when MediaError.MEDIA_ERR_NETWORK
				@_error "Check your network connectivity."
			when MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED
				@_error 'This format is not supported. Sorry.'
			when MediaError.MEDIA_ERR_DECODE
				@_error 'This content is possibly broken.'
			else
				@_error 'Could not play this content.'


# Lifetime management
# -------------------

	afterRender: ->
		# Reattach any players we may have left
		@$el.append @_player.$el if @_player

	beforeDispose: ->
		# Destroy the player before disposal of the view.
		@_detachPlayer @_player if @_player
