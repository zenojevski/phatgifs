Base = require 'base'
Utils = require 'utils'
PopcornPlayer = require './popcorn'


module.exports =
class HTML5VideoPlayer extends PopcornPlayer
	className: 'player popcorn-player html5video-player'

	@canPlay: (content) ->
		/\.(gifv|mp4|m4v|webm|mkv|ogv)\b/i.test content

	# Returns the intrinsic dimensions of the content.
	dimensions: ->
		if @_player
			width: @_player.media.videoWidth
			height: @_player.media.videoHeight
