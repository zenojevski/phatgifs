Base = require 'base'
Utils = require 'utils'
Player = require './player'


module.exports =
class UnknownPlayer extends Player

	className: 'player unknown-player'

	@canPlay: ->
		yes

	load: (content) ->
		@_error() if content
