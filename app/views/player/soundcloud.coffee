Base = require 'base'
Utils = require 'utils'
PopcornPlayer = require './popcorn'


module.exports =
class SoundCloudPlayer extends PopcornPlayer
	className: 'player popcorn-player soundcloud-player'

	@canPlay: (content) ->
		/\bsoundcloud\.com\b/.test content
