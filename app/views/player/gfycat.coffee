Base = require 'base'
Utils = require 'utils'
Settings = require 'settings'
Capsule = require './capsule'


# Gfycat
# ======

# Functions to operate withing the Gfycat service. Currently
# implemented are fetching of Gfycats and transcoding of GIFs.
Gfycat =

	# Returns the ID part of a Gfycat url.
	extractId: (url) ->
		(url.match /gfycat\.com\/(\w+)/)?[1]

	# Extracts the available video URLs from a Gfycat response
	extractAssets: (response) ->
		mp4:  response.mp4Url
		webm: response.webmUrl
		gif:  response.gifUrl

	# Fetches Gfycat assets from a gfycat URL. Will return an object
	# with `mp4`, `webm` and `gif` properties as made available by
	# gfycat. Returns the xhr object. Pass `success` and `error`
	# callbacks to obtain the response.
	fetch: (url, options) ->
		xhr = $.ajax
			url: "http://gfycat.com/cajax/get/#{Gfycat.extractId url}"
		xhr.error error = (x) =>
			options?.error? x.responseText
		xhr.done (r) =>
			return error (responseText:r.error) if r.error
			options?.success? Gfycat.extractAssets r.gfyItem

	# Transcodes a GIF image to a Gfycat video. Will return an object
	# with `mp4`, `webm` and `gif` properties, depending on the
	# outcome of the operation. Pass `success` and `error` callbacks
	# to obtain the response.
	transcode: (url, options) ->
		xhr = $.ajax
			url: "http://upload.gfycat.com/transcode"
			data: fetchUrl:(encodeURI url)
		xhr.error error = (x) ->
			options?.error? x.responseText
		xhr.done (r) ->
			return error (responseText:r.error) if r.error
			options?.success? Gfycat.extractAssets r


# Gfycat player
# =============

# Player class to play both GIF images and GfyCat urls.
module.exports =
class GfycatPlayer extends Capsule
	className: 'player capsule-player gfycat-player'

	# This player can accept Gfycat URLs and GIFs.
	@tests =
		gfycat: /\bgfycat\.com\b/
		image: /\.(gif|jpg|jpeg|png)\b/i

	# This player can play HTML5 videos and images.
	@players = [
		require './html5video'
		require './image'
	]

	# Overridden because the player converts between formats, and so
	# accepts different things than what it can ultimately play.
	@canPlay: (content) ->
		_.any @tests, (matcher) ->
			matcher.test content

	# Tries to fetch video assets from a Gfycat URL and play the best
	# available ones between _mp4_, _webm_, and _gif_.
	_loadGfycat: (content) ->
		Gfycat.fetch content,
			success: @_load
			error: @_error

	# Tries to transcode a GIF, and play the best transcoded format
	# produced between _mp4_, _webm_, and _gif_. If the transcoding
	# fails, falls back to the original GIF image.
	_transcodeGif: (content) ->
		Gfycat.transcode content,
			success: @_load
			error: => @_load gif:content

	# Actually produce a player and load a content. It will accept
	# an object containing video format, from which it will pick the
	# best one between _mp4_, _webm_, and _gif_.
	_load: (assets) =>
		asset = assets.webm or assets.mp4 or assets.gif
		@constructor.__super__.load.call this, asset # Call the original `load`

	# Tries to load a content, either by fetching it from Gfycat or
	# transcoding it to a more modern video format.
	load: (content) ->
		{gfycat, image} = @constructor.tests
		# Load a gfycat url directly.
		if gfycat.test content
			@_loadGfycat content
		# It's a gif, try to to transcode and load the video
		# or fallback to the gif
		else if image.test content
			@_transcodeGif content
		# What? We don't know how to manage this, but also we should
		# never end here.
		else @_error()

	afterRender: ->
		super
