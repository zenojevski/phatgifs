Base = require 'base'
Utils = require 'utils'
Capsule = require './capsule'


# A "smart" player class that switches the underlying player
# class based on which one is able to play the given content.
module.exports =
class Player extends Capsule

	# The list of available players, ordered by priority by which
	# they will be queried for the capability to play a content.
	@players = [
		require './youtube'
		require './vimeo'
		require './html5video'
		require './soundcloud'
		require './html5audio'
		require './gfycat'
		require './image'
	]

