Base = require 'base'
Utils = require 'utils'
BasePlayer = require './player'


# Image player
# ============

# This player can display various image types, first and foremost
# GIFs, but also the usual JPEGs and PNGs.
module.exports =
class ImagePlayer extends BasePlayer

	className: 'player image-player'

	# This player can display various image types.
	@canPlay: (content) ->
		/\.(gif|jpg|jpeg|png)\b/i.test content

	# Returns the currently loaded content.
	content: ->
		@_content

	# Returns the intrinsic dimensions of the content.
	dimensions: ->
		if @_image
			width: @_image.width
			height: @_image.height

	# Preload the image, signalling loading states throughout.
	load: (content) ->
		@ready = no
		Utils.images.preload content, @_load
		_.defer => @trigger 'loading'

	unload: (content) ->
		@_display no
		_.defer => @trigger 'unload'

	play: ->
		@playing = yes
		@_display @_content

	_load: (img, content) =>
		unless img and img.width and img.height
			return @_error 'Could not load this image.'
		@ready = yes
		@error = no
		@_image = img
		@_content = content
		@_display @_content
		@trigger 'load'
		@trigger 'canplay'

	_display: (content) ->
		if @playing and content
			@$content.append @_image
			@trigger 'play'
		else
			@$content.empty()
