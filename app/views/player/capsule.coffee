@Base = require 'base'
Utils = require 'utils'
BasePlayer = require './player'


# A "smart" player class that switches the underlying player
# class based on which one is able to play the given content.
module.exports =
class Capsule extends BasePlayer

	className: 'player-capsule'
	template: no

	# The list of available players, ordered by priority by which they
	# will be queried for the capability to play a content.
	@players: []

	# Tests each player, in order, and returns the first which is able
	# to play the content.
	@playerFor: (content) ->
		_.find @players, (Player) ->
			Player.canPlay content

	# Returns `true` if this player can play the content through one
	# of its delegated players.
	@canPlay: (content) ->
		(@playerFor content)?

	# The list of methods which the player should proxy to the
	# underlying instance.
	@proxiedMethods: [
		'dimensions'
		'duration'
		'currentTime'
		'cueStartTime'
		'cueEndTime'
		'play'
		'pause'
		'seek'
		'rewind'
		'rewindToCue'
	]

	# The list of events which the player should proxy from the
	# underlying instance.
	@proxiedEvents: [
		'loading'
		'load'
		'unload'
		'canplay'
		'play'
		'pause'
		'seek'
		'mute'
		'unmute'
		'error'
	]

	initialize: (options) ->
		# Keep the options around to instantiate new players.
		@options = options
		# Proxy some methods to the underlying players.
		@_proxyMethods()


# Media management
# ----------------

	# Returns the currently loaded content. This will always return
	# the original specified content url, even if changed by the
	# underlying player.
	content: ->
		@_content

	# Loads a content URL into the player. Will emit a `load` event
	# when the content is ready to be played, with the loaded content
	# as first argument.
	load: (content) ->
		unless content = @_cleanURL content
			return
		# Don't act if we didn't change the content.
		if content is @content()
			return
		# If we don't have a cached player, or the player is not able
		# to play the new content, make a new player instance.
		unless @_player?.constructor.canPlay content
			@_detachPlayer @_player if @_player
			if @_player = @_makePlayer content, @options
				@_attachPlayer @_player
				@_applyPlayerQueue @_player
			else
				return @_error "This format is not supported."
		else
			# Load the content into the player
			@_player.load content
		@_content = content

	# Unloads the current content from the player. Will emit an
	# `unload` event when the player is empty.
	unload: ->
		@error = no
		@ready = no
		@_player?.unload()
		@_detachPlayer @_player if @_player
		delete @_content
		delete @_player

	# Keep the cue values to be replayed to new instances.
	cue: (cues) ->
		@options.cues = cues
		@_callOnPlayer @_player, 'cue', cues

	# Keep the cue deletion to be replayed to new instances.
	uncue: ->
		delete @options.cues
		@_callOnPlayer @_player, 'uncue', cues

	# Keep the mute state to be replayed to new instances.
	mute: (muted) ->
		@options.muted = muted ? not @options.muted
		@_callOnPlayer @_player, 'mute', @options.muted


# Internals
# ---------

	# Builds a player instance
	_makePlayer: (content, options) ->
		if Player = @constructor.playerFor content
			new Player _.extend {}, options,
				content: content

	# Disposes, unbinds and detaches a player instance.
	_detachPlayer: (player) ->
		@stopListening player
		player.detach()

	# Attaches a player instance to the view, and proxies its events.
	_attachPlayer: (player) ->
		@attach player
		@_proxyEvents player

	# Proxy methods to the underlying instance.
	_proxyMethods: ->
		for method in @constructor.proxiedMethods then do (method) =>
			@[method] = (args...) =>
				@_callOnPlayer @_player, method, args

	# Proxy events from the underlying instance.
	_proxyEvents: (player) ->
		for event in @constructor.proxiedEvents then do (event) =>
			@listenTo player, event, (args...) =>
				# Update current player states
				@error = player.error
				@ready = player.ready
				@playing = player.playing
				# Trigger the same event on self.
				@trigger event, args...

	# Queues a method call on the player for later execution,
	# eg. when the player instance has been created.
	_callOnPlayer: (player, method, args...) ->
		# Proxy the command immediately if we have a player,
		# else queue it for later execution.
		if fn = player?[method]
			fn.apply @_player, args
		else
			@_queuePlayerCall method, args

	# Queues a method call on the player for later execution,
	# eg. when the player instance has been created.
	_queuePlayerCall: (method, args) ->
		@_playerCallQueue or= []
		@_playerCallQueue.push [method, args]

	# Execute any queued calls on the given player instance.
	_applyPlayerQueue: (player) ->
		return unless @_playerCallQueue?.length
		for [method, args] in @_playerCallQueue
			player[method] args...
		@_playerCallQueue = []

	afterRender: ->
		# Reattach any players we may have left
		@_attachPlayer @_player if @_player

	beforeDispose: ->
		@_detachPlayer @_player if @_player
