Base = require 'base'
Utils = require 'utils'
PopcornPlayer = require './popcorn'


module.exports =
class HTML5AudioPlayer extends PopcornPlayer
	className: 'player popcorn-player html5audio-player'

	@canPlay: (content) ->
		/\.(mp3|mp4|m4a|aac|ogg)\b/i.test content
