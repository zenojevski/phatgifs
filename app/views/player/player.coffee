Base = require 'base'
Utils = require 'utils'


# Base class for an URL-based player.
# - `muted`:    The player will start muted.
# - `autoplay`: The player will start playing as soon as possible
# - `cues`:     The player will loop around the given range of time.
# - `content`:  Load the content into the player when ready.
module.exports =
class BasePlayer extends Base.View

	className: 'player'
	omittedOptions: ['muted', 'autoplay', 'content', 'cues']
	template: require 'templates/player/base'

	ui:
		$content: '.player-content'

	constructor: (options) ->
		if options.muted
			@muted = yes
		if options.autoplay
			@playing = yes
		super
		if options.content
			@load options.content
		if options.cues
			@cue options.cues

	# Triggers and logs an error with a given reason.
	_error: (reason) =>
		@error = reason
		console?.error reason
		@trigger 'error', reason


# Content management
# ------------------

	# Test if this player is able to read a given content. Return
	# `true` if the player can play the URL, or `false` otherwise.
	@canPlay: (content) ->
		no

	# Returns a cleaned version of the content URL. Can also be used
	# to apply transformations, eg. transform URL to canonical form.
	_cleanURL: (content) ->
		Utils.text.trim content

	# Returns the currently loaded content.
	content: ->

	# Returns the intrinsic dimensions of the content.
	dimensions: ->

	# Loads a content URL into the player. Will emit a `load` event
	# when the content is ready to be played, with the loaded content
	# as first argument.
	load: (content) ->
		@ready = no
		@trigger 'loading'
		_.defer =>
			@error = no
			@ready = yes
			@trigger 'load'
			_.defer => @trigger 'canplay'

	# Unloads the current content from the player. Will emit an
	# `unload` event when the player is empty.
	unload: ->
		@error = no
		@ready = no
		_.defer => @trigger 'unload'


# Media control
# -------------

	# Returns the length of the currently loaded content.
	duration: ->
		return 0

	# Returns the current playing time.
	currentTime: ->
		return 0

	# Tells the player to start reproducing the content. Will trigger
	# a `play` event when the content has begun playing.
	play: ->
		@playing = yes
		_.defer => @trigger 'play'

	# Tells the player to pause the reproduction of the content. Will
	# trigger a `pause` event when the content has paused.
	pause: ->
		@playing = no
		_.defer => @trigger 'pause'

	# Seeks the player to a certain position in time. The position is
	# limited to the maximum duration of the content. Will emit a
	# `seek` event, with the current time as argument.
	seek: (position) ->
		_.defer => @trigger 'seek', @currentTime()

	# Rewinds the content to the beginning. Will trigger both a
	# `rewind` and a `seek` event with the current time as argument.
	rewind: ->
		@seek 0

	# Mutes the player. Pass a boolean as argument to set the mute
	# switch to the given value, or null to flip the current setting.
	# Will trigger a `mute` or `unmute` event based on the value.
	mute: (muted=null) ->
		@muted = muted ? not @muted
		_.defer => @trigger if muted then 'mute' else 'unmute'


# Cueing
# ------

	# Returns the time of the start cue.
	cueStartTime: -> 0

	# Returns the time of the end cue.
	cueEndTime: -> @duration()

	# Returns `true` if the player is currently inside the cues.
	isInCueRange: ->
		((t=@currentTime()) >= @cueStartTime()) and (t <= @cueEndTime())

	# Rewinds the content to the starting cue position. Will trigger
	# both a `rewind` and a `seek` event with the current time as
	# argument.
	rewindToCue: -> @seek @cueStartTime()

	# Sets the cues which constrain reproduction.
	cue: (cues={}) ->

	# Removes any set cues.
	uncue: ->
