Base = require 'base'
Utils = require 'utils'
PopcornPlayer = require './popcorn'


module.exports =
class YoutubePlayer extends PopcornPlayer
	className: 'player popcorn-player youtube-player'

	@canPlay: (content) ->
		/\b(youtube\.com|yt\.be)\b/.test content

	# Sent when an error occurs.
	_onPlayerError: ->
		switch (err = @_player.error).code
			when MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED
				@_error 'This YouTube video is private or it has been deleted. Sorry.'
			else
				super
