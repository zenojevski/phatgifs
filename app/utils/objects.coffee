Function::property = (prop, desc) ->
	Object.defineProperty @prototype, prop, desc


Objects =

	clean: (obj) ->
		result = {}
		for key, value of obj
			if value then result[key] = value
		return result

	pickOptions: (obj, options={}, whitelist=[]) ->
		_.extend obj, Objects.clean (_.pick options, whitelist)

module.exports = Objects