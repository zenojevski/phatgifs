events =
	wrap: (ctx, method, event=method) ->
		oldMethod = ctx[method]
		ctx[method] = (args...) ->
			@trigger "#{event}:before", args...
			result = oldMethod.apply ctx, args
			@trigger "#{event}", args...
			return result

	delegate: (ctx, property, target) ->
		for event, method of ctx[property]
			@listenTo target, event, ctx[method]
	undelegate: (ctx, property, target) ->
		for event, method of ctx[property]
			@stopListening target, event, ctx[method]

	bind: (ctx, property, target) ->
		for event, method of ctx[property]
			target.on event, ctx[method], ctx
	unbind: (ctx, property, target) ->
		for event, method of ctx[property]
			target.off event, ctx[method], ctx


module.exports = events