math =
	clamp: (min, val, max) ->
		Math.max min, (Math.min val, max)
	random: (from, to) ->
		Math.round (Math.random() * (to - from) + from)


module.exports = math