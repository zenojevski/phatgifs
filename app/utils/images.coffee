isGIFRe = /\.(gif)$/i
isImageRe = /\.(gif|jpg|jpeg|png)$/i
isGfycatRe = /^((https?:)?\/\/)?gfycat\.com\//
gfycatImageIdRe = /gfycat\.com\/(\w+)/

images =
	isGIF: (src) ->
		isGIFRe.test src
	isGfycat: (src) ->
		isGfycatRe.test src
	isImage: (src) ->
		isImageRe.test src

	gfycatImageId: (src) ->
		(src.match gfycatImageIdRe)?[1]

	preload: (src, callback, ctx) ->
		img = new Image
		img.onload = ->
			callback.call ctx, img, src
		img.onerror = img.onabort = ->
			callback.call ctx, false, src
		img.src = src

module.exports = images