strings =

	# Generates a random 10-chars token.
	uniqueToken: ->
		Math.random().toString()[2..12]

	capitalize: (str) ->
		"#{(str.charAt 0).toUpperCase()}#{str.slice 1}"

module.exports = strings