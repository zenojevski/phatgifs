module.exports = text =

	# Trims a string of its initial and final whitespace.
	# Currently just wraps jQuery's own `$.trim`.
	trim: (str) ->
		$.trim str