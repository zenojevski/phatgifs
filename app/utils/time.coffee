timeCueRe = /(?:([\d.]+)h)?(?:([\d.]+)m)?(?:([\d.]+)s?)?\b/

time =

	parseCue: (cue) ->
		if (tokens = timeCueRe.exec cue)?.length
			h = ((parseFloat tokens[1], 10) or 0) * 3600
			m = ((parseFloat tokens[2], 10) or 0) * 60
			s = ((parseFloat tokens[3], 10) or 0) * 1
			return h+m+s

	toCue: (s) ->
		cue = ""
		if h = s % 3600
			s -= h * 3600
			cue += "#{h}h"
		if m = s % 60
			s -= m * 60
			cue += "#{m}m"
		cue += "#{s}s"


module.exports = time